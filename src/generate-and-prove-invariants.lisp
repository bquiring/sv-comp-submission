
; ==================
; ----- Traces -----
; ==================

; node * (list [node list] [edge list]) -> [node list]
(defun get-successors (graph node)
  (list.filter #'(lambda (edge)
		   (equal (edge.from edge) node))
	       (cadr graph)))

; N state node graph [node [assignment list] mmap] -> '_
; assumes mmap is defined on every node
(defun generate-trace (fuel state node graph mmap)
  (if (< fuel 0)
      '_
    (let* ((successors (get-successors graph node))
	   (vars (node.live-vars node))
	   (succ-opt (list.find #'(lambda (edge)
				    (equal (funcall (definec.name (edge.condition edge))
						    state)
					   t))
			     successors)))
      (if (option.exists succ-opt)
	  (let* ((succ-edge (option.get succ-opt))
		 (succ (edge.to succ-edge))
		 (vars* (node.live-vars succ))
		 (state* (funcall (definec.name (edge.statement succ-edge)) state))
		 (assignment (list.foldr2 #'(lambda (var-pair value acc)
					      (if (not (equal (car var-pair) 'nondet))
						  (cons (pair.make (car var-pair) value)
							acc)
						acc))
					  () vars* state*)))
	    (mmap.add-overwrite mmap (node.name succ) 
				(cons assignment (mmap.find-exn mmap (node.name succ))))
	    (generate-trace (- fuel 1) state* succ graph mmap))
	'_))))
	       
; (list [node list] [edge list]) N N node -> [node-name [assignment list] mmap]
(defun generate-traces (graph fuel num start-node)
  (let* ((mmap (mmap.empty))
	 (_ (list.iter #'(lambda (node) (mmap.add-exn mmap (node.name node) ())) (car graph)))
	 (datap (name-to-recognizer (defdata.name (node.defdata start-node))))
	 (start-states (list.map #'(lambda (assignment)
				    (cadr (car assignment)))
				(acl2s-generate-counter-examples `(implies (,datap input) nil) num)))
	 (_ (list.iter #'(lambda (start-state) (generate-trace fuel start-state *start-node* graph mmap)) start-states)))
    mmap))



; ======================
; ----- Invariants -----
; ======================

; [equality list] -> [equality list]
(defun inv-gen-sort-equalities (eqs)
  (list.sort eqs
	     #'(lambda (eq1 eq2)
		 (let* ((lhs1 (equality.lhs eq1))
			(rhs1 (equality.rhs eq1))
			(lhs2 (equality.lhs eq2))
			(rhs2 (equality.rhs eq2))
			(type1 (term.type lhs1))
			(type2 (term.type lhs2))
			(ls1 (term.size lhs1))
			(rs1 (term.size rhs1))
			(ls2 (term.size lhs2))
			(rs2 (term.size rhs2)))
		   (or (and (not (equal type1 type2))
			    (cond ((and (equal type1 'booleanp)
					(equal type1 'integerp))
				   t)
				  ((and (equal type1 'integerp)
					(equal type1 'booleanp))
				   nil)))
		       (and (equal type1 type2)
			    (or (and (= (min ls1 rs1) (min ls2 rs2))
				     (> (+ ls1 rs1) (+ ls2 rs2)))
				(> (min ls1 rs1) (min ls2 rs2)))))))))

; [hyp list] * [equality list] -> [equality list]
(defun inv-gen-find-minimal (req-hyps eqs)
  (if (endp eqs)
      ()
    (let* ((e (car eqs))
	   (eqs* (inv-gen-find-minimal req-hyps (cdr eqs))))
      (if (acl2s-thm (list 'implies 
			   (cons 'and (append req-hyps (list.map #'equality.destruct eqs*)))
			   (equality.destruct e)))
	  eqs*
	(cons e eqs*)))))

; N N [(list value ACL2-type) list] [(list ACL2-var ACL2-type) list] [(list ACL2-funvar (list [ACL2-type list] ACL2-type)) list] -> [ACL2-expression list]
(defun generate-invariants (n k consts vars funs traces
		       &key (guards ()) (do-boolean nil) (minimize t) (filters ()) (uncond-assignments ()) (req-hyps ()) (debug nil) (print-cache-summary nil))
  (assert (<= k n))
  (let* ((consts* (list.map #'(lambda (stuff) (cons (multiple-value-bind (x y) (gentemp "c") x) stuff)) consts))
	 (const-lookup (create-const-lookup consts*))
	 (var-lookup (create-var-lookup vars))
	 (fun-lookup (create-fun-lookup funs))
	 (const-syms (list.map #'car consts*))
	 (var-syms (list.map #'car vars))
	 (fun-syms (list.map #'car funs)) 
	 (req-hyps (append (create-req-hyps vars) req-hyps))
	 (all-types (all-types const-lookup var-lookup fun-lookup))
	 (comparisons (list.foldr #'(lambda (type acc) (map.add-overwrite acc type (list type))) map.empty all-types))
	 (subtypes ())
	 (S (S.create const-syms var-syms fun-syms
		      const-lookup var-lookup fun-lookup
		      (map.from-list (list.map #'(lambda (stuff) (pair.make (car stuff) (cadr stuff))) consts*))
		      all-types req-hyps (map.from-list guards) (create-subtypes all-types subtypes) (create-supertypes all-types subtypes) comparisons))
	 (uncond-assignments (append (create-assignments req-hyps 100) uncond-assignments))
	 ;(_ (print (list 'req-hyps req-hyps)))
	 ;(_ (print (list 'initial-assignments uncond-assignments)))
	 (initial-cache (create-initial-cache S uncond-assignments 0))
	 (res-1 (construct-all-terms-and-equalities n initial-cache S 0 filters))
	 (term-cache (pair.first res-1))
	 (uncond-eqs (map.fold #'(lambda (_ eqs acc) (append (list.map #'equality.destruct eqs) acc)) 
				   () (pair.second res-1)))

	 (eqs (inv-gen-sort-equalities (pair.second (find-eqs term-cache traces S 1 :ignore-bool t))))
	 (eqs* (if minimize (inv-gen-find-minimal req-hyps eqs) eqs)))
    eqs*))

; N N [(list ACL2-var ACL2-type) list] [assignment list] -> [ACL2-expression list]
(defun generate-invariants-header (n k live-vars traces)
  (generate-invariants n k
		       '((0 c-unsigned-intp) (1 c-unsigned-intp))
		       (list.map #'(lambda (var-pair) (list (car var-pair)
							    (ACL2-type-to-recognizer (cadr var-pair))))
				 live-vars)
		       '((c-unsigned-int-+ ((c-unsigned-intp c-unsigned-intp) c-unsigned-intp))
			 (c-unsigned-int-* ((c-unsigned-intp c-unsigned-intp) c-unsigned-intp))
		       )
		       traces))


; ===================================
; ----- Verification Conditions -----
; ===================================

#|
To prove an invariant at a node, you case over the nodes coming into it
along with the conditions from the edges.

the proof obligation for proving I_{n, i} is
for all states S, for all m ---> n with condition c and statement s, 
  c(S) and I_{m, 1}(S) and ... and I_{m, k}(S)) implies I_{n, i}(s(S))

So if some I_{n, i} cannot be proven, then
we remove it from the pool and try again with all proof obligations
We can actually remove _all_ unproved obligations at every stage

|#

#|
; (list [node list] [edge list]) [node-name [ACL2-expression list] map] -> [ACL2-expression list]
(defun generate-verification-conditions (graph invariant-map)
  (list.foldr #'(lambda (node acc)
		  (let* ((invariants (map.find-exn invariant-map (node.name node)))
			 (predecessors (list.filter #'(lambda (edge) (equal (edge.to edge) node)) (cadr graph)))
			 (live-vars (node.live-vars node)))
		    (list.foldr #'(lambda (edge acc)
				    (let* ((node* (edge.from edge))
					   (live-vars* (node.live-vars node*))
					   (invariants* (map.find-exn invariant-map (node.name node*)))
					   (condition-name (definec.name (edge.condition edge)))
					   (statement-name (definec.name (edge.statement edge)))
					   (recognizer* (name-to-recognizer (defdata.name (node.defdata node*))))
					   (hypotheses `(and (,recognizer* state)
							     (,condition-name state)
							     ,(use-fill-in-context 'state
										   (list.map #'car live-vars*)
										   (cons 'and invariants*))))
					   (single-invariant #'(lambda (inv)
								 `(implies ,hypotheses
									   ,(use-fill-in-context (list statement-name 'state)
												 (list.map #'car live-vars)
												 inv)))))
				      (list.foldr #'(lambda (inv acc)
						      (cons (apply single-invariant (list inv)) acc))
						  acc invariants)))
				acc predecessors)))
	      () (car graph)))
|#

; (list [node list] [edge list]) [node-name [ACL2-expression list] map] -> [ACL2-expression list]
(defun generate-verification-conditions-single (graph invariant-map node invariant)
  (let* ((predecessors (list.filter #'(lambda (edge) (equal (edge.to edge) node)) (cadr graph)))
	 (live-vars (node.live-vars node)))
    (list.foldr #'(lambda (edge acc)
		    (let* ((node* (edge.from edge))
			   (live-vars* (node.live-vars node*))
			   (invariants* (map.find-exn invariant-map (node.name node*)))
			   (condition-name (definec.name (edge.condition edge)))
			   (statement-name (definec.name (edge.statement edge)))
			   (recognizer* (name-to-recognizer (defdata.name (node.defdata node*))))
			   (hypotheses `(and (,recognizer* state)
					     (,condition-name state)
					     ,(use-fill-in-context 'state
								   (list.map #'car live-vars*)
								   (cons 'and invariants*)))))
		      (cons `(implies ,hypotheses
				      ,(use-fill-in-context (list statement-name 'state)
							    (list.map #'car live-vars)
							    invariant))
			    acc)))
		() predecessors)))

; (list [node list] [edge list]) node-name -> node
(defun find-node (graph node-name)
  (option.get (list.find #'(lambda (node) (equal (node.name node) node-name)) (car graph))))


; (list [node list] [edge list]) [node-name [ACL2-expression list] map] node ACL2-expression -> boolean
(defun satisfies-all-verification-conditions (graph potential-invariants node invariant)
  (let* ((verif-conds (generate-verification-conditions-single graph potential-invariants node invariant)))
    (list.forall #'(lambda (verif-cond)
		     (acl2s-thm verif-cond :prover-step-limit 300000))
		 verif-conds)))
  
; (list [node list] [edge list]) [node-name [ACL2-expression list] map] -> [node-name [ACL2-expression list] map]
(defun prove-invariants (graph potential-invariants)
  (let* ((unproven-invariants (map.map #'(lambda (node-name invariants)
					   (let* ((node (find-node graph node-name)))
					     (list.filter
					      #'(lambda (invariant)
						  (satisfies-all-verification-conditions graph potential-invariants node invariant))
					      invariants)))
				       potential-invariants)))
    (if all-proven
	potential-invariants
      (let* ((pruned-invariants 
	      (map.map #'(lambda (node-name invariants)
			   (let* ((unproven (map.find-exn unproven-invariants node-name)))
			     (list.filter #'(lambda (invariant)
					      (not (list.in invariant unproven)))
					  invariants)))
		       potential-invariants)))
	(generate-and-prove-invariants graph pruned-invariants)))))

; [node-name [ACL2-expression list] map] [node-name ACL2-expression map] -> [node-name ACL2-expression map]
(defun prove-assertions (proven-invariants assertion-map)
  (map.fold #'(lambda (node-name assertion unproven)
		(let* ((invariants (map.find-exn proven-invariants node-name)))
		  (if (acl2s-thm (list 'implies (cons 'and invariants) assertion))
		      unproven
		    (map.add-exn unproven node-name assertion))))
	    () assertion-map))

; ==================
; ----- Driver -----
; ==================

(defparameter *start-node* (node.make '|node-start| '((nondet nondet) (counter nat)) (create-defdata '((nondet nondet) (counter nat)))))
(defparameter *end-node* (node.make '|node-end| '((nondet nondet) (counter nat)) (create-defdata '((nondet nondet) (counter nat)))))

(define-condition not-all-assertions-proved (error)
  ((text :initarg :text :reader text)))

(defun create-graph/admit-functions/generate-trace/find-invariants (src-prgm architecture fuel &key (verbose nil) (print-acl2s-defs nil))
  (acl2s-admit-c-defs architecture verbose)
  (let* ((prgm (convert-program src-prgm))
	 (name-map (statement-name-map prgm map.empty))
	 (res (statement-to-acl2-statement* prgm *end-node* option.none (list *end-node*) () map.empty *end-node*))
	 (assertions (cadddr (cdr res)))
	 (_ (if verbose (print (list 'assertions assertions)) '_))
	 (node (car res))
	 (enter-loop-head (cadr res))
	 (start-stm (make-trivial-stm (node.live-vars *start-node*) (node.defdata *start-node*)
				      (node.live-vars node) (node.defdata node)))
	 (condition
	  `(definec ,(condition-name) (input ,(name-to-type (defdata.name (node.defdata *start-node*)))) :boolean
	     t))
	 (start-edge (edge.make (edge-name) *start-node* node condition start-stm '(loc :start-line 0 :end-line 0 :offset 0 :length 0)
				:enter-loop-head enter-loop-head :source-code option.none))
	 (graph (list (cons *start-node* (caddr res))
		      (cons start-edge (cadddr res))))
	 (ACL2-defdatas (list.map #'(lambda (node) (node.defdata node)) (car graph)))
	 (ACL2-conditions (list.map #'(lambda (edge) (edge.condition edge)) (cadr graph)))
	 (ACL2-statements (list.map #'(lambda (edge) (edge.statement edge)) (cadr graph)))
	 (_ (list.iter #'(lambda (defdata) 
			   (let* ((admit (acl2s-event defdata)))
			     (if verbose
				 (if (not (car admit))
				     (print (list 'admitted defdata))
				   (print (list 'failed defdata)))
			       '_)))
		       ACL2-defdatas))
	 (_ (list.iter #'(lambda (condition)
			   (let* ((admit (acl2s-event condition)))
			     (if verbose
				 (if (not (car admit))
				     (print (list 'admitted condition))
				   (print (list 'failed condition)))
			       '_))
			   (acl2s-event `(in-theory (enable ,(definec.name condition)))))
		       ACL2-conditions))
	 (_ (list.iter #'(lambda (statement) 
			   (let* ((admit (acl2s-event statement)))
			     (if verbose
				 (if (not (car admit))
				     (print (list 'admitted statement))
				   (print (list 'failed statement)))
			       '_))
			   (acl2s-event `(in-theory (enable ,(definec.name statement)))))
		       ACL2-statements))
	 (mmap (generate-traces graph fuel 10 *start-node*))
	 (_ (if verbose (mmap.iter #'(lambda (key traces) (print (list key (length traces)))) mmap) '_))
	 (_ (if verbose (print '==================) '_))
	 (_ (if verbose (list.iter #'(lambda (node) (print node)) (car graph)) '_))
	 (_ (if verbose (list.iter #'(lambda (edge) (print (list (node.name (edge.from edge))
								 '--
								 (definec.name (edge.condition edge))
								 (definec.name (edge.statement edge))
								 '->
								 (node.name (edge.to edge)))))
				   (cadr graph))
	      '_))
	 (invariant-map (list.foldr #'(lambda (node acc) 
					(let* ((traces (mmap.find-exn mmap (node.name node)))
					       (live-vars (list.filter #'(lambda (var-pair)
									   (and (not (equal (car var-pair) 'nondet)) (not (equal (car var-pair) 'counter))))
								       (node.live-vars node)))
					       (invariants 
						(if (or (endp live-vars) (endp traces))
						    ()
						  (generate-invariants-header 7 1 live-vars traces))))
					  (if verbose (print (list (node.name node) (list.map #'equality.destruct invariants))) '_)
					  (map.add-exn acc (node.name node) (list.map #'equality.destruct invariants))))
				    map.empty (car graph)))
	 (_ (if (and verbose print-acl2s-defs) (print '<><><><><><><><><><><>)))
	 (_ (if print-acl2s-defs (list.iter #'(lambda (event) (print event)) *c-defs*) '_))
	 (_ (if print-acl2s-defs (list.iter #'(lambda (event) (print event)) *c-nondet-defs*) '_))
	 (_ (if print-acl2s-defs (list.iter #'(lambda (node) (print (node.defdata node))) (car graph)) '_))
	 (_ (if print-acl2s-defs (list.iter #'(lambda (edge) (print (edge.condition edge))) (cadr graph)) '_))
  	 (_ (if print-acl2s-defs (list.iter #'(lambda (edge) (print `(in-theory (enable ,(definec.name (edge.condition edge)))))) (cadr graph)) '_))
	 (_ (if print-acl2s-defs (list.iter #'(lambda (edge) (print (edge.statement edge))) (cadr graph)) '_))
  	 (_ (if print-acl2s-defs (list.iter #'(lambda (edge) (print `(in-theory (enable ,(definec.name (edge.statement edge)))))) (cadr graph)) '_))
	 (_ (if verbose (print (list 'potential 'invariants)) '_))
	 (_ (if verbose (map.iter #'(lambda (node-name invariants) (list.iter #'(lambda (invariant) (print (list node-name invariant))) invariants)) invariant-map) '_))
	 (proven-invariants (prove-invariants graph invariant-map))
	 (_ (if verbose (print (list 'proven 'invariants)) '_))
	 (_ (if verbose (map.iter #'(lambda (node-name invariants) (list.iter #'(lambda (invariant) (print (list node-name invariant))) invariants)) proven-invariants) '_))
	 (unproven-assertions (prove-assertions proven-invariants assertions))
	 (_ (if verbose
		(if (endp unproven-assertions)
		    (print (list 'all 'assertions 'proven))
		  (progn (print (list 'unproven 'assertions))
			 (map.iter #'(lambda (node-name assertion) (print (list node-name assertion))) unproven-assertions)))
	      '_))
	 (_ (if (not (endp unproven-assertions)) (error 'not-all-assertions-proved) '_))
	 (witness-graph (create-witness-graph graph proven-invariants *start-node* name-map assertions)))
    witness-graph))


#|
(load "./invariant-generation/load.lisp")
(declaim (sb-ext:muffle-conditions cl:warning))
; TODO: what happens if we can't end up finding any ways to reach the invariant? (i.e. fuel too small)
(create-graph/admit-functions/generate-trace/find-invariants *program-1* 10000 :verbose t)

(create-graph/admit-functions/generate-trace/find-invariants (convert-program *c-program-1*) 10000 :verbose t) 
|#
