
#|

The language that is given from the Eclipse C Parser is

'alpha maybe = '_ | 'a

program := (list 'program ':declarations [declaration list])
loc := (list 'loc :start-line int :end-line int :offset int :length int)

declaration := (list 'simple-declaration 
                     ':declarators [declarator list]
                     ':decl-specifier decl-specifier 
                     ':loc loc)
             | (list 'function-definition 
                     ':declarator declarator 
                     ':decl-specifier decl-specifier 
                     ':body statement 
                     ':loc loc)

binary-operator := 'assign 
                 | 'bin-and | 'assign-bin-and | 'bin-or | 'assign-bin-or | 'bin-xor | 'assign-bin-xor 
                 | '== | '!= 
                 | '>= | '> | '<= | '< 
                 | 'log-and | 'log-or 
                 | '+ | 'assign-+ | '- | 'assign-- | '* | 'assign-* | '/ | 'assign-/ 
                 | 'mod | 'assign-mod 
                 | '<< | '<<= | '>> | '>>=

unary-operator := 'reference | 'dereference 
                | '- | '+ 
                | 'log-not | 'bin-not 
                | 'decr-post | 'incr-post | 'decr-pre | 'incr-pre 
                | 'parens ; wrapping an expression in parens is treated as a unary operator

decl-type-base := 'auto | 'bool | 'char | 'int | 'float | 'double | 'void
decl-type := (list 'decl-type
		   ':base decl-type-base
		   ':modifiers [type-modifier list])

type-kind := 'bool | 'char | 'char16 | 'char32 | 'double | 'float | 'int | 'void

type-modifier := 'long | 'long-long | 'short | 'signed | 'unsigned | 'complex | 'imaginary

type-id = (list 'type-id
                ':abstract-declarator declarator
                ':decl-specifier decl-specifier)

type := (list 'basic-type
              :modifier [type-modifier list]
              :kind type-kind)
      | (list 'function-type
              ':input-types [type list]
              ':return-type type)
      | (list 'array-type 
              ':type type) 
      | (list 'pointer-type 
              ':type type) 

name := (list 'name symbolp) ; TODO: watch out for t / nil / other reserved?
      | (list 'unnamed)

literal-kind := 'char-constant
              | 'false-constant
              | 'float-constant
              | 'integer-constant
              | 'string-constant
              | 'true-constant

expression := (list 'literal-expression
                    :type type
                    :kind literal-kind
                    :value TODO
                    :loc loc)
            | (list 'id-expression
                    ':type type
                    ':name name
                    ':loc loc)
            | (list 'unary-expression
                    ':type type
                    ':operator unary-operator
                    ':operand expression
                    ':loc loc)
            | (list 'binary-expression
                    ':type type
                    ':operator binary-operator
                    ':operand1 expression
                    ':operand2 expression
                    ':loc loc)
            | (list 'function-call-expression
                    ':type type
                    ':function expression
                    ':arguments [expression list]
                    ':loc loc)
            | (list 'compound-statement-expression
                    ':type type
                    ':statements [statement list]
                    ':loc loc)
            | (list 'conditional-expression
                    ':type type
                    ':test expression
                    ':then expression
                    ':else expression
                    ':loc loc)
            | (list 'array-subscript-expression
                    ':type type
                    ':array expression
                    ':subscript [expression maybe]
                    ':arg [expression maybe]
                    ':loc loc)
            | (list 'cast-expression
                    ':type type
                    ':operator type-id
                    ':operand expression
                    ':loc loc)

statement := (list 'compound-statement
                   ':statements [statement list]
                   ':loc loc)
           | (list 'return-statement
                   ':expression [expression maybe]
                   ':loc loc)
           | (list 'if-statement
                   ':test expression
                   ':then statement
                   ':else [statement maybe]
                   ':loc loc)
           | (list 'while-statement
                   ':test expression
                   ':body statement
                   ':loc loc)
           | (list 'do-statement
                   ':test expression
                   ':body statement
                   ':loc loc)
           | (list 'for-statement
                   ':init statement
                   ':test expression
                   ':iter expression
                   ':body statement
                   ':loc loc)
           | (list 'break-statement
                   ':loc loc)
           | (list 'continue-statement
                   ':loc loc)
           | (list 'default-statement
                   ':loc loc)
           | (list 'null-statement
                   ':loc loc)
           | (list 'goto-statement
                   ':label name
                   ':loc loc)
           | (list 'declaration-statement
                   ':declaration declaration
                   ':loc loc)
           | (list 'expression-statement
                   ':expression expression
                   ':loc loc)
           | (list 'case-statement
                   ':expression expression
                   ':loc loc)
           | (list 'switch-statement
                   ':controller-expression expression
                   ':body statement
                   ':loc loc)
           | (list 'label-statement
                   ':label name
                   ':nested [expression maybe]
                   ':loc loc)
                    
initializer-clause := (list 'initializer-clause-expression 
                            ':expression expression
                            ':loc loc)

initializer := (list 'initializer
                     ':initializer-clause initializer-clause
                     ':loc loc)

declarator := (list 'function-declarator
                    ':name name
                    ':parameters [parameter-declaration list]
                    ':initializer [initializer maybe]
                    ':loc loc)
            | (list 'declarator
                    ':name name
                    ':initializer [initializer maybe]
                    ':loc loc)

parameter-declaration := (list 'parameter-declaration 
                               ':declarator declarator
                               ':decl-specifier decl-specifier
                               ':loc loc)

decl-specifier := (list 'decl-specifier 
                        ':decl-type decl-type
                        ':loc loc)


|#


#|

Future:
program := (list 'program ':declarations [declaration list]) 
declaration := (list 'simple-declaration 
                     ':declaring declaring
                     ':initializer expression
                     ':loc loc)
             | (list 'function-definition 
		     ':name name
                     ':parameters [declaring list]
                     ':return-type type
                     ':body statement 
                     ':loc loc)
expression 
| (list 'function-call-expression
        ':type type
        ':function name
        ':arguments [expression list]
        ':loc loc)
| (list 'compound-statement-expression
        ':type type
        ':statements [statement list]
        ':loc loc)

lvalue
| (list 'array-lvalue 
        ':lvalue array
        ':subscript expression)
| (list 'array-subscript-expression
        ':type type
        ':array expression
        ':subscript expression
        ':loc loc)
| (list 'cast-expression
        ':type type
        ':operator type-id
        ':operand expression
        ':loc loc)

statement
| (list 'expression-statement
        ':expression expression
        ':loc loc)



The language I handle:

program := statement

loc := (list 'loc :start-line int :end-line int :offset int :length int)

declaring := (list 'declaring
		   ':var var
		   ':type type)

binary-operator := '== | '!= 
                 | '>= | '> | '<= | '< 
                 | 'log-and | 'log-or 
                 | '+ | '- | '* | '/ 
                 | 'unsigned-+ | 'unsigned-- | 'unsigned-* | 'unsigned-/ 
                 | 'mod 

unary-operator := '- | 'log-not 

base-type := 'bool | 'char | 'int | 'unsigned-int | 'void

type := (list 'basic-type
              :kind base-type)
      | (list 'array-type 
              ':type type) 

literal-kind := 'char-constant
              | 'false-constant
              | 'float-constant
              | 'integer-constant
              | 'string-constant
              | 'true-constant

expression := (list 'literal-expression
                    :type type
                    :value value
                    :loc loc)
            | (list 'id-expression
                    ':type type
                    ':var var
                    ':loc loc)
            | (list 'unary-expression
                    ':type type
                    ':operator unary-operator
                    ':operand expression
                    ':loc loc)
            | (list 'binary-expression
                    ':type type
                    ':operator binary-operator
                    ':operand1 expression
                    ':operand2 expression
                    ':loc loc)
	    | (list 'nondeterministic
                    ':type type
                    ':loc loc) 
            | (list 'conditional-expression
                    ':type type
                    ':test expression
                    ':then expression
                    ':else expression
                    ':loc loc)

statement := (list 'compound-statement
                   ':statements [statement list]
                   ':loc loc)
           | (list 'if-then-statement
                   ':test expression
                   ':then statement
                   ':loc loc)
           | (list 'if-then-else-statement
                   ':test expression
                   ':then statement
                   ':else statement
                   ':loc loc)
           | (list 'while-statement
                   ':test expression
                   ':body statement
                   ':loc loc)
           | (list 'do-statement
                   ':test expression
                   ':body statement
                   ':loc loc)
           | (list 'for-statement
                   ':init statement
                   ':test expression
                   ':iter statement
                   ':body statement
                   ':loc loc)
           | (list 'declaration-statement
                   ':declaring declaring
		   ':initializer expression
                   ':loc loc)
           | (list 'assignment-statement
                   ':lvalue lvalue
                   ':expression expression
                   ':loc loc)
           | (list 'assert-statement
                   ':expression expression
                   ':loc loc)

lvalue := (list 'var-lvalue 
                ':type type
                ':var var)

|#

#|
; type -> ?
(defun type-template (type)
  (cond ((equal (car type) 'basic-type)
	 (cond ((equal (l.find type ':kind) 'bool)
		...)
	       ((equal (l.find type ':kind) 'char)
		...)
	       ((equal (l.find type ':kind) 'int)
		...)
	       ((equal (l.find type ':kind) 'unsigned-int)
		...)
	       ((equal (l.find type ':kind) 'void)
		...)))
	((equal (car type) 'array-type)
	 ...)
	))

; expression -> ?
(defun expression-template (exp)
  (cond ((equal (car exp) 'literal-expression)
	 ...)
	((equal (car exp) 'id-expression)
	 ...)
	((equal (car exp) 'unary-expression)
	 ...)
	((equal (car exp) 'binary-expression)
	 ...)
	((equal (car exp) 'nondeterministic-expression)
	 ...)
	((equal (car exp) 'conditional-expression)
	 ...)
	))

; statement -> ?
(defun statement-template (stm)
  (cond ((equal (car stm) 'compound-statement)
	 ...)
	((equal (car stm) 'if-then-statement)
	 ...)
	((equal (car stm) 'if-then-else-statement)
	 ...)
	((equal (car stm) 'while-statement)
	 ...)
	((equal (car stm) 'do-statement)
	 ...)
	((equal (car stm) 'for-statement)
	 ...)
	((equal (car stm) 'declaration-statement)
	 ...)
	((equal (car stm) 'assignment-statement)
	 ...)
	))

|#

; name-map := [symbol (list ACL2-var symbol)]

(define-condition no-conversion (error)
  ((text :initarg :text :reader text)))

; decl-type -> type
(defun convert-decl-type (decl-type)
  (let* ((modifiers (l.find decl-type ':modifiers)))
    (if (or (list.in 'long modifiers)
	    (list.in 'long-long modifiers)
	    (list.in 'short modifiers)
	    (list.in 'signed modifiers)
	    (list.in 'complex modifiers)
	    (list.in 'imaginary modifiers))
	(error 'no-conversion)
      (cond ((equal (l.find decl-type ':base) 'bool)
	     (if (list.in 'unsigned modifiers)
		 (error 'no-conversion)
	       (list 'basic-type ':kind 'bool)))
	    ((equal (l.find decl-type ':base) 'char)
	     (if (list.in 'unsigned modifiers)
		 (error 'no-conversion)
	       (list 'basic-type ':kind 'char)))
	    ((equal (l.find decl-type ':base) 'int)
	     (if (list.in 'unsigned modifiers)
		 (list 'basic-type ':kind 'unsigned-int)
	       (list 'basic-type ':kind 'int)))
	    ((equal (l.find decl-type ':base) 'double)
	     (error 'no-conversion))
	    ((equal (l.find decl-type ':base) 'float)
	     (error 'no-conversion))
	    ((equal (l.find decl-type ':base) 'void)
	     (error 'no-conversion))
	    ((equal (l.find decl-type ':base) 'auto)
	     (error 'no-conversion))))))

; c-type -> type
(defun convert-type (type)
  (cond ((equal (car type) 'basic-type)
	 (let* ((modifiers (l.find type ':modifiers)))
	   (if (or (list.in 'long modifiers)
		   (list.in 'long-long modifiers)
		   (list.in 'short modifiers)
		   (list.in 'signed modifiers)
		   (list.in 'complex modifiers)
		   (list.in 'imaginary modifiers))
	       (error 'no-conversion)
	     (cond ((equal (l.find type ':kind) 'bool)
		    (if (list.in 'unsigned modifiers)
			(error 'no-conversion)
		      (list 'basic-type ':kind 'bool)))
		   ((equal (l.find type ':kind) 'char)
		    (if (list.in 'unsigned modifiers)
			(error 'no-conversion)
		      (list 'basic-type ':kind 'char)))
		   ((equal (l.find type ':kind) 'int)
		    (if (list.in 'unsigned modifiers)  
			(list 'basic-type ':kind 'unsigned-int)
		      (list 'basic-type ':kind 'int)))
		   ((equal (l.find type ':kind) 'double)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'float)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'char16)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'char32)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'void)
		    (error 'no-conversion))))))
	((equal (car type) 'function-type)
	 (error 'no-conversion))
	((equal (car type) 'array-type)
	 (error 'no-conversion))
	((equal (car type) 'pointer-type)
	 (error 'no-conversion))))

; [expression (list 'not-an-expression statement) U] -> expression
(defun coerce-expression-to-expression (exp)
  (if (equal (car exp) 'not-an-expression)
      (error 'no-conversion)
    exp))

; [expression (list 'not-an-expression statement) U] -> statement
(defun coerce-expression-to-statement (exp)
  (if (equal (car exp) 'not-an-expression)
      (cadr exp)
    (error 'no-conversion)))

; [statement (list 'not-an-statement expression) U] -> statement
(defun coerce-statement-to-statement (stm)
  (if (equal (car stm) 'not-a-statement)
      (error 'no-conversion)
    stm))

; [statement (list 'not-a-statement expression) U] -> expression
(defun coerce-statement-to-expression (stm)
  (if (equal (car stm) 'not-a-statement)
      (cadr stm)
    (error 'no-conversion)))

(defun coerce-expression-to-type (expression type)
  (let* ((type* (l.find expression ':type))
	 (unsigned-int-type '(basic-type :kind unsigned-int))
	 (int-type '(basic-type :kind int))
	 (char-type '(basic-type :kind char))
	 (bool-type '(basic-type :kind bool))
	 (loc (l.find expression ':loc)))
    (cond ((equal (car expression) 'literal-expression)
	   (cond ((equal type type*)
		  expression)
		 ((and (equal type unsigned-int-type)
		       (equal type* int-type))
		  (list 'literal-expression
			':value (mod (l.find expression ':value) *c-unsigned-int-size*)
			':type type
			':source-code (l.find expression ':source-code)
			':loc loc))
		 ((and (equal type int-type)
		       (equal type* unsigned-int-type))
		  (list 'literal-expression
			':value (l.find expression ':value) 
			':type type
			':source-code (l.find expression ':source-code)
			':loc loc))
		 (t (error 'no-conversion))))

	  ((equal (car expression) 'id-expression)
	   (if (not (equal type type*))
	       (error 'no-conversion)
	     expression))

	  ((equal (car expression) 'unary-expression)
	   (cond ((equal (l.find expression ':operator) '-)
		  (cond ((equal type unsigned-int-type)
			 (list 'unary-expression
			       ':type type
			       ':operator '-
			       ':operand (coerce-expression-to-type (l.find expression ':operand) type)
			       ':source-code (l.find expression ':source-code)
			       ':loc loc))
			((equal type int-type)
			 (list 'unary-expression
			       ':type type
			       ':operator '-
			       ':operand (coerce-expression-to-type (l.find expression ':operand) type)
			       ':source-code (l.find expression ':source-code)
			       ':loc loc))
			((equal type char-type)
			 (error 'no-conversion))
			((equal type bool-type)
			 (error 'no-conversion))))
		 ((equal (l.find expression ':operator) 'log-not)
		  (cond ((equal type unsigned-int-type)
			 (error 'no-conversion))
			((equal type int-type)
			 (error 'no-conversion))
			((equal type char-type)
			 (error 'no-conversion))
			((equal type bool-type)
			 (list 'unary-expression
			       ':type type
			       ':operator 'log-not
			       ':operand (coerce-expression-to-type (l.find expression ':operand) type)
			       ':source-code (l.find expression ':source-code)
			       ':loc loc))))))
	  ((equal (car expression) 'binary-expression)
	   (let* ((operator (l.find expression ':operator))
		  (operand1 (l.find expression ':operand1))
		  (operand2 (l.find expression ':operand2)))
	     (cond ((equal operator '==)
		    (cond ((equal type unsigned-int-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (handler-case (list 'binary-expression
					       ':type type
					       ':operator operator
					       ':operand1 (coerce-expression-to-type operand1 unsigned-int-type)
					       ':operand2 (coerce-expression-to-type operand2 unsigned-int-type)
					       ':source-code (l.find expression ':source-code)
					       ':loc loc)
			     (no-conversion (se)
					    (handler-case (list 'binary-expression
								':type type
								':operator operator
								':operand1 (coerce-expression-to-type operand1 int-type)
								':operand2 (coerce-expression-to-type operand2 int-type)
								':source-code (l.find expression ':source-code)
								':loc loc)
					;TODO: more types
					      (no-conversion (se)
							     (error 'no-conversion))))))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((equal operator '!=)
		    (cond ((equal type unsigned-int-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (handler-case (list 'binary-expression
					       ':type type
					       ':operator operator
					       ':operand1 (coerce-expression-to-type operand1 unsigned-int-type)
					       ':operand2 (coerce-expression-to-type operand2 unsigned-int-type)
					       ':source-code (l.find expression ':source-code)
					       ':loc loc)
			     (no-conversion (se)
					    (handler-case (list 'binary-expression
								':type type
								':operator operator
								':operand1 (coerce-expression-to-type operand1 int-type)
								':operand2 (coerce-expression-to-type operand2 int-type)
								':source-code (l.find expression ':source-code)
								':loc loc)
					;TODO: more types
					      (no-conversion (se)
							     (error 'no-conversion))))))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '>=) (equal operator '>) (equal operator '<=) (equal operator '<))n
		    (cond ((equal type unsigned-int-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (handler-case (list 'binary-expression
					       ':type type
					       ':operator operator
					       ':operand1 (coerce-expression-to-type operand1 unsigned-int-type)
					       ':operand2 (coerce-expression-to-type operand2 unsigned-int-type)
					       ':source-code (l.find expression ':source-code)
					       ':loc loc)
			     (no-conversion (se)
					    (handler-case (list 'binary-expression
								':type type
								':operator operator
								':operand1 (coerce-expression-to-type operand1 int-type)
								':operand2 (coerce-expression-to-type operand2 int-type)
								':source-code (l.find expression ':source-code)
								':loc loc)
					      (no-conversion (se)
							     (error 'no-conversion))))))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator 'log-and) (equal operator 'log-or))
		    (cond ((equal type unsigned-int-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (list 'binary-expression
				 ':type bool-type
				 ':operator operator
				 ':operand1 (coerce-expression-to-type operand1 bool-type)
				 ':operand2 (coerce-expression-to-type operand2 bool-type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '+) (equal operator 'unsigned-+))
		    (cond ((equal type unsigned-int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned-+
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '+
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type bool-type)
			   (error 'no-conversion))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '-) (equal operator 'unsigned--))
		    (cond ((equal type unsigned-int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned--
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '-
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type bool-type)
			   (error 'no-conversion))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '*) (equal operator 'unsigned-*))
		    (cond ((equal type unsigned-int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned-*
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '*
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type bool-type)
			   (error 'no-conversion))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '/) (equal operator 'unsigned-/))
		    (cond ((equal type unsigned-int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned-/
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '/
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code (l.find expression ':source-code)
				 ':loc loc))
			  ((equal type bool-type)
			   (error 'no-conversion))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((equal operator 'mod)
		    (error 'no-conversion)))))
	  
	  ((equal (car expression) 'nondeterministic-expression)
	   (list 'nondeterministic-expression
		 ':type type
		 ':source-code (l.find expression ':source-code)
		 ':loc loc))
	   #|(cond ((equal type type*)
		  expression)
		 (t (error 'no-conversion))))|#
	  ((equal (car expression) 'conditional-expression)
	   (list 'conditional-expression
		 ':type type
		 ':test (coerce-expression-to-type (l.find expression ':test) bool-type)
		 ':then (coerce-expression-to-type (l.find expression ':then) type)
		 ':else (coerce-expression-to-type (l.find expression ':else) type)
		 ':source-code (l.find expression ':source-code)
		 ':loc loc))
	  )))


; c-program -> statement 
(defun convert-program (src-prgm)
  (let* ((declarations (l.find src-prgm ':declarations))
	 (main-opt (list.find #'(lambda (declaration)
				  (and (equal (car declaration) 'function-definition)
				       (equal (car (l.find declaration ':declarator)) 'function-declarator)
				       (equal (car (l.find (l.find declaration ':declarator) ':name)) 'name)
				       (equal (cadr (l.find (l.find declaration ':declarator) ':name)) 'main)))
			      declarations)))
    (if (option.exists main-opt)
	(coerce-statement-to-statement (pair.first (convert-statement (l.find (option.get main-opt) ':body) map.empty)))
      (error 'no-conversion))))

; c-declaration loc name-map -> [statement name-map pair]
(defun create-declaration-statement (declaration loc names)
  (cond ((equal (car declaration) 'simple-declaration)
	 (if (equal (length (l.find declaration ':declarators)) 1)
	     (let* ((declarator (car (l.find declaration ':declarators)))
		    (decl-spec (l.find declaration ':decl-specifier)))
	       (cond ((equal (car declarator) 'declarator)
		      (let* ((name (let* ((name-opt (l.find declarator ':name)))
				     (cond ((equal (car name-opt) 'name)
					    (cadr name-opt))
					   ((equal (car name-opt) 'unnamed)
					    (error 'no-conversion)))))
			     (initializer (l.find declarator ':initializer))
			     (type (convert-decl-type (l.find decl-spec ':decl-type)))
			     (init-exp (if (equal initializer '_)
					   (list 'nondeterministic-expression
						 ':type type
						 ':loc loc)
					 (coerce-expression-to-expression (convert-expression (l.find (l.find initializer ':initializer-clause) ':expression) names))))
			     (ACL2-var (var-name))
			     (declaring (list 'declaring ':var (list ACL2-var name) ':type type))
			     (dec-stm (list 'declaration-statement
					    ':declaring declaring
					    ':initializer (coerce-expression-to-type init-exp type)
					    ':source-code (c-declaration-to-source-code declaration)
					    ':loc loc)))
			(pair.make dec-stm (map.add-overwrite names name (list ACL2-var name)))))
		     ((equal (car declarator) 'function-declarator)
		      (error 'no-conversion))))
	     (error 'no-conversion)))
	((equal (car declaration) 'function-declaration)
	 (error 'no-conversion))))

; c-binary-op c-expression c-expression name-map -> [expression (list 'not-an-expression statement) U]
(defun convert-binary-expression (op exp1 exp2 type loc source-code names)
  (cond ((equal op 'assign)
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names))
		      (type (l.find lvalue ':type))
		      (expression (coerce-expression-to-type (coerce-expression-to-expression (convert-expression exp2 names)) type)))
		 (list 'assignment-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op 'bin-and) 
	 (error 'no-conversion))
	((equal op 'assign-bin-and) 
	 (error 'no-conversion))
	((equal op 'bin-or) 
	 (error 'no-conversion))
	((equal op 'assign-bin-or) 
	 (error 'no-conversion))
	((equal op 'bin-xor) 
	 (error 'no-conversion))
	((equal op 'assign-bin-xor) 
	 (error 'no-conversion))
	((equal op '==) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '==
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op '!=) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '!=
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op '>=) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '>=
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op '>) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '>
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op '<=) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '<=
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op '<) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '<
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'log-and) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'log-and
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'log-or) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'log-or
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op '+) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '+
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-+) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ;TODO
					 ':operator '+
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list 'assign-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op '-) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '-
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ;TODO
					 ':operator '-
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list 'assign-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op '*) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '*
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-*) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ;TODO
					 ':operator '*
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list 'assign-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op '/) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '/
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-/) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ;TODO
					 ':operator '/
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list 'assign-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op 'mod) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'mod
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-mod) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ;TODO
					 ':operator 'mod
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list 'assign-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op '<<) 
	 (error 'no-conversion))
	((equal op '<<=)
	 (error 'no-conversion))
	((equal op '>>) 
	 (error 'no-conversion))
	((equal op '>>=)
	 (error 'no-conversion))))

; c-unary-op c-exp c-type source-code loc -> [expression (list 'not-an-expression statement) U]
(defun convert-unary-expression (op exp type loc source-code names)
  (cond ((equal op 'reference) 
	 (error 'no-conversion))
	((equal op 'dereference)
	 (error 'no-conversion))
	((equal op '-) 
	 (list 'unary-expression
	       ':type (convert-type type)
	       ':operator '-
	       ':operand (coerce-expression-to-expression exp)
	       ':source-code source-code
	       ':loc loc))
	((equal op '+) (convert-expression exp names))
	((equal op 'log-not)
	 (list 'unary-expression
	       ':type (convert-type type)
	       ':operator 'log-not
	       ':operand (coerce-expression-to-expression exp)
	       ':source-code source-code
	       ':loc loc))
	((equal op 'bin-not) 
	 (error 'no-conversion))
	((equal op 'decr-post)
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '-
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type) #|TODO|# ':loc loc
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list 'assignment-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op 'incr-post) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '+
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type) #|TODO|# ':loc loc
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list 'assignment-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op 'decr-pre) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '-
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type) #|TODO|# ':loc loc
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list 'assignment-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op 'incr-pre) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '+
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type) #|TODO|# ':loc loc
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list 'assignment-statement
		       ':lvalue lvalue
		       ':expression expression
		       ':source-code source-code
		       ':loc loc))))
	((equal op 'parens) 
	 (convert-expression exp names))))


; c-expression -> lvalue
(defun convert-lvalue (exp names)
  (cond ((equal (car exp) 'literal-expression)
	 (error 'no-conversion))
	((equal (car exp) 'id-expression)
	 (if (equal (car (l.find exp ':name)) 'name)
	     (list 'var-lvalue
		   ':type (convert-type (l.find exp ':type))
		   ':var (map.find-exn names (cadr (l.find exp ':name)))
		   ':source-code (c-expression-to-source-code exp))
	   (error 'no-conversion)))
	((equal (car exp) 'unary-expression)
	 (error 'no-conversion))
	((equal (car exp) 'binary-expression)
	 (error 'no-conversion))
	((equal (car exp) 'function-call-expression)
	 (error 'no-conversion))
	((equal (car exp) 'compound-statement-expression)
	 (error 'no-conversion))
	((equal (car exp) 'conditional-expression)
	 (error 'no-conversion))
	((equal (car exp) 'array-subscript-expression)
	 (error 'no-conversion))
	((equal (car exp) 'cast-expression)
	 (error 'no-conversion))))


; c-expression ? -> [expression (list 'not-an-expression statement) U]
(defun convert-expression (exp names)
  (cond ((equal (car exp) 'literal-expression)
	 (list 'literal-expression
	       ':type (convert-type (l.find exp ':type)) ':loc (l.find exp ':loc)
	       ':value (l.find exp ':value)
	       ':source-code (c-expression-to-source-code exp)))

	((equal (car exp) 'id-expression)
	 (let* ((name (l.find exp ':name)))
	   (if (equal (car name) 'name)
	       (list 'id-expression
		     ':type (convert-type (l.find exp ':type)) ':loc (l.find exp ':loc)
		     ':var (map.find-exn names (cadr name))
		     ':source-code (c-expression-to-source-code exp))

	     (error 'no-conversion))))
	((equal (car exp) 'unary-expression)
	 (convert-unary-expression (l.find exp ':operator)
				   (l.find exp ':operand)
				   (l.find exp ':type)
				   (l.find exp ':loc)
				   (c-expression-to-source-code exp)
				   names))
	((equal (car exp) 'binary-expression)
	 (convert-binary-expression (l.find exp ':operator)
				    (l.find exp ':operand1)
				    (l.find exp ':operand2)
				    (l.find exp ':type)
				    (l.find exp ':loc)
				    (c-expression-to-source-code exp)
				    names))
	((equal (car exp) 'function-call-expression)
	 (let* ((func (l.find exp ':function)))
	   (cond ((and (equal (car func) 'id-expression)
		       (equal (car (l.find func ':name)) 'name))
		  (let* ((name (cadr (l.find func ':name)))
			 (loc (l.find exp ':loc)))
		    (cond ((equal name '__VERIFIER_nondet_uint)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'unsigned-int) ':loc loc
				 ':source-code (c-expression-to-source-code exp)))
			  ((equal name '__VERIFIER_nondet_int)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'int) ':loc loc
				 ':source-code (c-expression-to-source-code exp)))
			  ((equal name '__VERIFIER_nondet_bool)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'bool) ':loc loc
				 ':source-code (c-expression-to-source-code exp)))
			  ((equal name '__VERIFIER_nondet_char)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'char) ':loc loc
				 ':source-code (c-expression-to-source-code exp)))


			  ((equal name '__VERIFIER_assert)
			   (let* ((assert-exp (coerce-expression-to-expression (convert-expression (l.find (car (l.find exp ':arguments)) ':expression) names))))
			     (list 'not-an-expression
				   (list 'assert-statement ':expression assert-exp ':loc loc
					 ':source-code (c-expression-to-source-code exp)))))


			  (t (error 'no-conversion)))))
		  (t (error 'no-conversion)))))
	((equal (car exp) 'compound-statement-expression)
	 (list 'not-an-expression
	       (list 'compound-statement
		     ':statements 
		     (reverse 
		      (pair.first
		       (list.foldl #'(lambda (stm* acc)
				       (let* ((stms (pair.first acc))
					      (names* (pair.second acc))
					      (res (convert-statement stm* names*)))
					      (stm** (coerce-statement-to-statement (pair.first res))
					      (names** (pair.second res)))
					 (pair.make (cons stm** stms)
						    names**)))
				   (pair.make () names) (l.find exp ':statements))))
		     ':loc (l.find exp ':loc)
		     ':source-code (c-expression-to-source-code exp))))
	((equal (car exp) 'conditional-expression)
	 (list 'conditional-expression
	       ':type (convert-type (l.find exp ':type)) ':loc (l.find exp ':loc)
	       ':test (coerce-expression-to-expression (convert-expression (l.find exp ':test) names))
	       ':then (coerce-expression-to-expression (convert-expression (l.find exp ':then) names))
	       ':else (coerce-expression-to-expression (convert-expression (l.find exp ':else) names))
	       ':source-code (c-expression-to-source-code exp)))
	((equal (car exp) 'array-subscript-expression)
	 (error 'no-conversion))
	((equal (car exp) 'cast-expression)
	 (error 'no-conversion))))

; c-statement name-map -> [[statement (list 'not-a-statement expression) U] name-map pair]
(defun convert-statement (stm names)
  (cond ((equal (car stm) 'compound-statement)
	 (let* ((res (list.foldl #'(lambda (stm* acc)
				     (let* ((stms (pair.first acc))
					    (names* (pair.second acc))
					    (res (coerce-statement-to-statement (convert-statement stm* names*)))
					    (stm** (pair.first res))
					    (names** (pair.second res)))
				       (pair.make (cons stm** stms)
						  names**)))
				 (pair.make () names) (l.find stm ':statements)))
		(statements (reverse (pair.first res))))
	   (pair.make (list 'compound-statement
			    ':statements statements
			    ':loc (l.find stm ':loc)
			    ':source-code (c-statement-to-source-code stm))
		      names)))
	((equal (car stm) 'return-statement)
	 (let* ((return-exp (if (equal (l.find stm ':expression) '_)
				(literal-expression ':type '(basic-type ':kind int) ':value 0 ':loc (l.find stm ':loc)
						    ':source-code (c-statement-to-source-code stm))
			      (coerce-expression-to-expression (convert-expression (l.find stm ':expression) names)))))
	   (pair.make (list 'return-statement
			    ':expression return-exp
			    ':loc (l.find stm ':loc)
			    ':source-code (c-statement-to-source-code stm))
		      names)))
	((equal (car stm) 'if-statement)
	 (if (equal (l.find stm ':else) '_)
	     (let* ((test-exp (coerce-expression-to-type (coerce-expression-to-expression (convert-expression (l.find stm ':test) names)) '(basic-type :kind bool)))
		    (then-stm (coerce-statement-to-statement (pair.first (convert-statement (l.find stm ':then) names))))
		    (if-then-stm (list 'if-then-statement
				     ':test test-exp
				     ':then then-stm
				     ':loc (l.find stm ':loc)
				     ':source-code (c-statement-to-source-code stm))))
	       (pair.make if-then-stm names))
	   (let* ((test-exp (coerce-expression-to-type (coerce-expression-to-expression (convert-expression (l.find stm ':test) names)) '(basic-type :kind bool)))
		  (then-stm (coerce-statement-to-statement (pair.first (convert-statement (l.find stm ':then) names))))
		  (else-stm (coerce-statement-to-statement (pair.first (convert-statement (l.find stm ':else) names))))
		  (if-then-else-stm (list 'if-then-statement
				     ':test test-exp
				     ':then then-stm
				     ':else else-stm
				     ':loc (l.find stm ':loc)
				     ':source-code (c-statement-to-source-code stm))))
	     (pair.make if-then-else-stm names))))
	((equal (car stm) 'while-statement)
	 (let* ((test-exp (coerce-expression-to-type (coerce-expression-to-expression (convert-expression (l.find stm ':test) names))  '(basic-type :kind bool)))
		(body-stm (coerce-statement-to-statement (pair.first (convert-statement (l.find stm ':body) names))))
		(while-stm (list 'while-statement
				 ':test test-exp
				 ':body body-stm
				 ':loc (l.find stm ':loc)
				 ':source-code (c-statement-to-source-code stm))))
	   (pair.make while-stm names)))
	((equal (car stm) 'do-statement)
	 (let* ((test-exp (coerce-expression-to-type (coerce-expression-to-expression (convert-expression (l.find stm ':test) names)) '(basic-type :kind bool)))
		(body-stm (coerce-statement-to-statement (pair.first (convert-statement (l.find stm ':body) names))))
		(do-stm (list 'do-statement ':test test-exp ':body body-stm ':loc (l.find stm ':loc)
			      ':source-code (c-statement-to-source-code stm))))
	   (pair.make do-stm names)))
	((equal (car stm) 'for-statement)
	 (let* ((init-res (convert-statement (l.find stm ':init) names))
		(init-stm (coerce-statement-to-statement (pair.first init-res)))
		(names* (pair.second init-res))
		(test-exp (coerce-expression-to-type (coerce-expression-to-expression (convert-expression (l.find stm ':test) names*)) '(basic-type :kind bool)))
		(iter-stm (coerce-expression-to-statement (convert-expression (l.find stm ':iter) names*)))
		(body-stm (coerce-statement-to-statement (pair.first (convert-statement (l.find stm ':body) names*))))
		(for-stm (list 'for-statement ':init init-stm ':test test-exp ':iter iter-stm ':body body-stm ':loc (l.find stm ':loc)
			       ':source-code (c-statement-to-source-code stm))))
	   (pair.make for-stm names)))
	((equal (car stm) 'break-statement)
	 (error 'no-conversion))
	((equal (car stm) 'continue-statement)
	 (error 'no-conversion))
	((equal (car stm) 'default-statement)
	 (error 'no-conversion))
	((equal (car stm) 'null-statement)
	 (error 'no-conversion))
	((equal (car stm) 'goto-statement)
	 (error 'no-conversion))
	((equal (car stm) 'declaration-statement)
	 (create-declaration-statement (l.find stm ':declaration) (l.find stm ':loc) names))
	((equal (car stm) 'expression-statement)
	 (pair.make
	  (let* ((res (convert-expression (l.find stm ':expression) names)))
	    (if (equal (car res) 'not-an-expression)
		(coerce-expression-to-statement res)
	      (list 'not-a-statement (coerce-expression-to-expression res))))
	  names))
	((equal (car stm) 'case-statement)
	 (error 'no-conversion))
	((equal (car stm) 'switch-statement)
	 (error 'no-conversion))
	((equal (car stm) 'label-statement)
	 (error 'no-conversion))))




