
; requires "c-reasoner-utils.lisp"
; requires "base.lisp"

; var := (list ACL2-var program-var)

(defun var.ACL2-var (var)
  (pair.first var))

(defun var.program-var (var)
  (pair.second var))

; ======================================

; TODO: floor division, and unsigned operators, and maybe mod and stuff...

(defun unary-operator-to-acl2-unary-operator (op)
  (cond ((equal op '-) '-)
	((equal op 'log-not) 'not)
	(t (progn (print (list 'unimplemented 'unary 'op op))
		  (error 'unimplemented)))))

(defun binary-operator-to-acl2-binary-operator (op)
  (cond ((equal op '+) 'c-int-+)
	((equal op '-) 'c-int--)
	((equal op '*) 'c-inct-*)
	((equal op '/) 'c-int-/)
	((equal op 'unsigned-+) 'c-unsigned-int-+)
	((equal op 'unsigned--) 'c-unsigned-int--)
	((equal op 'unsigned-*) 'c-unsigned-int-*)
	((equal op 'unsigned-/) 'c-unsigned-int-/)
	((equal op '==) 'equal)
	((equal op '!=) 'notequal)
	((equal op '>=) '>=)
	((equal op '>) '>)
	((equal op '<=) '<=)
	((equal op '<) '<)
	((equal op 'log-and) 'and)
	((equal op 'log-or) 'or)
	;((equal op 'mod) 'mod)
	(t (progn (print (list 'unimplemented 'binary 'op op))
		  (error 'unimplemented)))))


; type -> ACL2-type
(defun type-to-acl2-type (type)
  (cond ((equal (car type) 'basic-type)
	 (cond ((equal (l.find type ':kind) 'bool)
		'c-bool)
	       ((equal (l.find type ':kind) 'char)
		'c-char)
	       ((equal (l.find type ':kind) 'int)
		'c-int)
	       ((equal (l.find type ':kind) 'unsigned-int)
		'c-unsigned-int)))))

(defun type-to-acl2-type-recognizer (type)
  (cond ((equal (car type) 'basic-type)
	 (cond ((equal (l.find type ':kind) 'bool)
		'c-boolp)
	       ((equal (l.find type ':kind) 'char)
		'c-charp)
	       ((equal (l.find type ':kind) 'int)
		'c-intp)
	       ((equal (l.find type ':kind) 'unsigned-int)
		'c-unsigned-intp)))))

(defun result-name ()
  (gentemp "r"))
(defun expression-name ()
  (gentemp "e"))

(defun name-to-type (name)
  (intern (string name) "KEYWORD"))
(defun name-to-recognizer (name)
  (intern (string-append (string name) "P")))
(defun ACL2-type-to-recognizer (type)
  (cond ((equal type 'c-bool) 'c-boolp)
	((equal type 'c-char) 'c-charp)
	((equal type 'c-int) 'c-intp)
	((equal type 'c-unsigned-int) 'c-unsigned-intp)
	((equal type 'c-void) 'c-voidp)))


; ACL2-expression [ACL2-var list] ACL2-expression -> ACL2-expression
(defun to-acl2-fill-in-context (exp args-to-pull-out hole)
  (let* ((result-var (result-name))
	 (cdr-list (cdr (list.map #'(lambda (_) 'cdr) args-to-pull-out)))
	 (pull-outs (pair.first (list.foldr
				 #'(lambda (var acc)
				     (let* ((acc-list (pair.first acc))
					    (cdr-list (pair.second acc))
					    (cdr-d-result (list.foldr
							   #'(lambda (x acc) (cons x (list acc)))
							   result-var cdr-list)))
				       (pair.make (cons `(,var (car ,cdr-d-result))
							acc-list)
						  (cdr cdr-list))))
				 (pair.make () cdr-list) args-to-pull-out))))
    `(let* ,(cons (list result-var exp)
		  pull-outs)
       ,hole)))

; ALC2-expression [ACL2-type list] -> ACL2-expression 
(defun create-output-contract (call types)
  (let* ((cdr-list (cdr (list.map #'(lambda (_) 'cdr) types))))
    (append (list 'and
		  `(true-listp ,call)
		  `(endp ,(list.foldr
			   #'(lambda (x acc) (cons x (list acc)))
			   call (cons 'cdr cdr-list))))
	    (pair.first (list.foldl 
			 #'(lambda (type acc)
			     (let* ((acc-list (pair.first acc))
				    (cdr-list (pair.second acc))
				    (cdr-d-call (list.foldr
						 #'(lambda (x acc) (cons x (list acc)))
						 call cdr-list)))
			       (pair.make (cons `(,type (car ,cdr-d-call))
						acc-list)
					  (cdr cdr-list))))
			 (pair.make () cdr-list) types)))))


; symbol [ACL2-type list] -> ACL2-defdata
(defun defdata.make (name types)
  `(defdata ,name ,(cons 'list types)))
; ACL2-defdata -> symbol
(defun defdata.name (defdata)
  (cadr defdata))

; symbol ... -> ACL2.definec
(defun definec.make (name var-type-list output-type body)
  `(definec ,name ,var-type-list ,output-type ,body))
; ACL2-definec -> symbol
(defun definec.name (definec)
  (cadr definec))
; ACL2-definec -> symbol
(defun definec.body (definec)
  (cadddr definec))

; ['a list] ['a list] -> ['a list]
(defun list-intersect (l1 l2)
  (if (endp l1)
      ()
    (let ((res (list-intersect (cdr l1) l2)))
      (if (list.in (car l1) l2)
	  (cons (car l1) res)
	res))))

; ['a list] ['a list] -> ['a list]
(defun list-union (l1 l2)
  (if (endp l1)
      l2
    (let ((res (list-union (cdr l1) l2)))
      (if (list.in (car l1) l2)
	  res
	(cons (car l1) res)))))

; ============================


; expression -> [(list ACL2-var ACL2-type) list]
(defun expression-free-vars (exp)
  (cond ((equal (car exp) 'literal-expression)
	 ())
	((equal (car exp) 'id-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(var (var.ACL2-var (l.find exp ':var))))
	   (list (list var type))))
	((equal (car exp) 'unary-expression)
	 (expression-free-vars (l.find exp ':operand)))
	((equal (car exp) 'binary-expression)
	 (list-union (expression-free-vars (l.find exp ':operand1))
		     (expression-free-vars (l.find exp ':operand2))))
	((equal (car exp) 'nondeterministic-expression)
	 (list (list 'nondet 'nondet) (list 'counter 'nat)))
	((equal (car exp) 'conditional-expression)
	 (list-union (expression-free-vars (l.find exp ':test))
		     (list-union (expression-free-vars (l.find exp ':then))
				 (expression-free-vars (l.find exp ':else)))))
	))

; statement -> [(list ACL2-var ACL2-type) list]
(defun statement-free-vars (stm)
  (cond ((equal (car stm) 'compound-statement)
	 (list.foldr (lambda (stm acc)
		       (list-union (statement-free-vars stm) acc))
		     () (l.find stm ':statements)))
	((equal (car stm) 'if-then-statement)
	 (list-union (expression-free-vars (l.find stm ':test))
		     (statement-free-vars (l.find stm ':then))))
	((equal (car stm) 'if-then-else-statement)
	 (list-union (expression-free-vars (l.find stm ':test))
		     (list-union (statement-free-vars (l.find stm ':then))
				 (statement-free-vars (l.find stm ':else)))))
	((equal (car stm) 'while-statement)
	 (list-union (expression-free-vars (l.find stm ':test))
		     (statement-free-vars (l.find stm ':body))))
	((equal (car stm) 'do-statement)
	 (error 'unimplemented))
	((equal (car stm) 'for-statement)
	 (error 'unimplemented))
	((equal (car stm) 'break-statement)
	 (error 'unimplemented))
	((equal (car stm) 'continue-statement)
	 (error 'unimplemented))
	((equal (car stm) 'declaration-statement)
	 (expression-free-vars (l.find stm ':initializer)))
	((equal (car stm) 'assignment-statement)
	 (expression-free-vars (l.find stm ':expression)))
	((equal (car stm) 'expression-statement)
	 (error 'unimplemented))
	))

; expression -> ACL2-expression
(defun expression-to-ACL2-expression* (exp)
  (cond ((equal (car exp) 'literal-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(value (l.find exp ':value))
		(body value))
		body))

	((equal (car exp) 'id-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(var (var.ACL2-var (l.find exp ':var)))
		(body var))
	   body))
	
	((equal (car exp) 'unary-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(op (unary-operator-to-acl2-unary-operator (l.find exp ':operator)))
		(body1 (expression-to-acl2-expression* (l.find exp ':operand)))
		(body `(,op ,body1)))
	   body))

	((equal (car exp) 'binary-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(op (binary-operator-to-acl2-binary-operator (l.find exp ':operator)))
		(body1 (expression-to-acl2-expression* (l.find exp ':operand1)))
		(body2 (expression-to-acl2-expression* (l.find exp ':operand2)))
		(body `(,op ,body1 ,body2)))
	   body))

	((equal (car exp) 'nondeterministic-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type))))
	   (cond ((equal type 'c-bool)
		  '(nondet-c-bool nondet counter))
		 ((equal type 'c-char)
		  '(nondet-c-char nondet counter))
		 ((equal type 'c-unsigned-int)
		  '(nondet-c-unsigned-int nondet counter))
		 ((equal type 'c-int)
		  '(nondet-c-int nondet counter)))))

	((equal (car exp) 'conditional-expression)
	 (error 'unimplemented))
	))

#|
node = (list node-name [(list ACL2-var ACL2-type) list] [(list ACL2-var ACL2-type) list] ACL2-defdata)
edge = (list edge-name node node ACL2-definec ACL2-definec loc
             [bool option] [['condition-true 'condition-false U] option] [source-code option]
|#

(defun node.make (name live-vars defdata)
  (list name live-vars defdata))
(defun node.name (node)
  (car node))
(defun node.live-vars (node)
  (cadr node))
(defun node.defdata (node)
  (caddr node))

(defun edge.make (name node-from node-to condition stm loc &key (enter-loop-head option.none) (control option.none) (source-code option.none))
  (list name node-from node-to condition stm loc enter-loop-head control source-code))
(defun edge.name (edge)
  (car edge))
(defun edge.from (edge)
  (cadr edge))
(defun edge.to (edge)
  (caddr edge))
(defun edge.condition (edge)
  (cadddr edge))
(defun edge.statement (edge)
  (cadddr (cdr edge)))
(defun edge.loc (edge)
  (cadddr (cddr edge)))
(defun edge.enter-loop-head (edge)
  (cadddr (cdddr edge)))
(defun edge.control (edge)
  (cadddr (cdddr (cdr edge))))
(defun edge.source-code (edge)
  (cadddr (cdddr (cddr edge))))

; [(list ACL2-var ACL2-type) list] -> ACL2-defdata
(defun create-defdata (live-vars)
  `(defdata ,(defdata-name) ,(cons 'list (list.map #'cadr live-vars))))

; ACL2-expression [ACL2-var list] ACL2-expression -> ACL2-expression
(defun use-fill-in-context (exp args-to-pull-out hole)
  (let* ((cdr-list (cdr (list.map #'(lambda (_) 'cdr) args-to-pull-out)))
	 (pull-outs (pair.first (list.foldr
				 #'(lambda (var acc)
				     (let* ((acc-list (pair.first acc))
					    (cdr-list (pair.second acc))
					    (cdr-d-result (list.foldr
							   #'(lambda (x acc) (cons x (list acc)))
							   exp cdr-list)))
				       (pair.make (cons `(,var (car ,cdr-d-result))
							acc-list)
						  (cdr cdr-list))))
				 (pair.make () cdr-list) args-to-pull-out))))
    `(let* ,pull-outs
       ,hole)))

; make the datadefs line up, essentially a projection
(defun make-trivial-stm (live-vars1 defdata1 live-vars2 defdata2)
  ; require subset
  ;(assert (list.forall #'(lambda (e2) (list.in e live-vars1)) live-vars2))
  `(definec ,(statement-name) (input ,(name-to-type (defdata.name defdata1))) ,(name-to-type (defdata.name defdata2))
     ,(use-fill-in-context 'input
			   (list.map #'car live-vars1)
			   (cons 'list (list.map #'(lambda (var) (if (equal var 'counter)
								     '(+ counter 1)
								   var))
						 (list.map #'car live-vars2))))))

; statement node [bool option] [node list] [edge list] [node-name ACL2-expression map] -> (list node [bool option] [node list] [edge list] [node-name ACL2-expression map])
; the created node; the list of created nodes; the list of created edges; the assertions that need to be proved
(defun statement-to-acl2-statement* (stm to-go-to enter-loop-head nodes edges assertions end-node)
  (cond ((equal (car stm) 'compound-statement)
	 (list.foldr #'(lambda (stm* acc)
		    (let* ((to-go-to (car acc))
			   (enter-loop-head (cadr acc))
			   (nodes (caddr acc))
			   (edges (cadddr acc))
			   (assertions (cadddr (cdr acc)))
			   (res (statement-to-acl2-statement* stm* to-go-to enter-loop-head nodes edges assertions end-node))
			   (returned (car res))
			   (enter-loop-head* (cadr res))
			   (nodes* (caddr res))
			   (edges* (cadddr res))
			   (assertions* (cadddr (cdr res))))
		      (list returned enter-loop-head* nodes* edges* assertions*)))
		(list to-go-to enter-loop-head nodes edges assertions) (l.find stm ':statements)))
	((equal (car stm) 'return-statement)
	 (let* ((n-name (node-name))
		(live-vars (list-union (node.live-vars end-node) (expression-free-vars (l.find stm ':expression))))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))
		(new-stm (make-trivial-stm live-vars defdata (node.live-vars end-node) (node.defdata end-node)))
		(condition
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    t))
		(new-edge (edge.make (edge-name) new-node end-node condition new-stm (l.find stm ':loc)
				     :enter-loop-head enter-loop-head :source-code (option.some (statement-to-source-code stm))))
		(nodes* (cons new-node nodes))
		(edges* (cons new-edge edges)))
	   (list new-node option.none nodes* edges* assertions)))
	((equal (car stm) 'if-then-statement)
	 (let* ((res-then (statement-to-acl2-statement* (l.find stm ':then) to-go-to enter-loop-head nodes edges assertions end-node))
		(then-node (car res-then))
		(enter-loop-head* (cadr res-then))
		(nodes* (caddr res-then))
		(edges* (cadddr res-then))
		(assertions* (cadddr (cdr res-then)))

		(test-exp (expression-to-acl2-expression* (l.find stm ':test)))
		(test-vars (expression-free-vars (l.find stm ':test)))

		(n-name (node-name))
		(live-vars (list-union (node.live-vars then-node) (list-union (node.live-vars to-go-to) test-vars)))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))

		(then-stm (make-trivial-stm live-vars defdata (node.live-vars then-node) (node.defdata then-node)))
		(other-stm (make-trivial-stm live-vars defdata (node.live-vars to-go-to) (node.defdata to-go-to)))

		(test-func
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    ,(use-fill-in-context 'input (list.map #'car live-vars) test-exp)))
		(neg-test-func
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    ,(use-fill-in-context 'input (list.map #'car live-vars) (list 'not test-exp))))
		
		(then-edge (edge.make (edge-name) new-node then-node test-func then-stm (l.find (l.find stm ':test) ':loc)
				      :enter-loop-head enter-loop-head*
				      :control (option.some 'condition-true) 
				      :source-code (option.some (expression-to-source-code (l.find stm ':test)))))
		(fall-through-edge (edge.make (edge-name) new-node to-go-to neg-test-func other-stm (l.find (l.find stm ':test) ':loc)
					      :enter-loop-head enter-loop-head 
					      :control (option.some 'condition-false) 
					      :source-code (option.some (string-append "!" (expression-to-source-code (l.find stm ':test))))))
		(nodes** (cons new-node nodes*))
		(edges** (cons then-edge (cons fall-through-edge edges*))))
	   (list new-node option.none nodes** edges** assertions*)))

	((equal (car stm) 'if-then-else-statement)
	 (let* ((res-then (statement-to-acl2-statement* (l.find stm ':then) to-go-to enter-loop-head nodes edges assertions end-node))
		(then-node (car res-then))
		(enter-loop-head* (cadr res-then))
		(nodes* (caddr res-then))
		(edges* (cadddr res-then))
		(assertions* (cadddr (cdr res-then)))

		(res-else (statement-to-acl2-statement* (l.find stm ':else) to-go-to enter-loop-head nodes* edges* assertions* end-node))
		(else-node (car res-else))
		(enter-loop-head** (cadr res-then))
		(nodes** (caddr res-else))
		(edges** (cadddr res-else))
		(assertions** (cadddr (cdr res-else)))

		(test-exp (expression-to-acl2-expression* (l.find stm ':test)))
		(test-vars (expression-free-vars (l.find stm ':test)))
		
		(n-name (node-name))
		(live-vars (list-union (node.live-vars then-node)
				       (list-union (node.live-vars else) test-vars)))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))

		(then-stm (make-trivial-stm live-vars defdata (node.live-vars then-node) (node.defdata then-node)))
		(else-stm (make-trivial-stm live-vars defdata (node.live-vars else-node) (node.defdata else-node)))

		(test-func
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    ,(use-fill-in-context 'input (list.map #'car live-vars) test-exp)))
		(neg-test-func
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    ,(use-fill-in-context 'input (list.map #'car live-vars) (list 'not test-exp))))

		(then-edge (edge.make (edge-name) new-node then-node test-func then-stm (l.find (l.find stm ':test) ':loc)
				      :enter-loop-head enter-loop-head*
				      :control (option.some 'condition-true) 
				      :source-code (option.some (expression-to-source-code (l.find stm ':test)))))
		(else-edge (edge.make (edge-name) new-node else-node neg-test-func else-stm (l.find (l.find stm ':test) ':loc)
				      :enter-loop-head enter-loop-head**
				      :control (option.some 'condition-false) 
				      :source-code (option.some (string-append "!" (expression-to-source-code (l.find stm ':test))))))
		(nodes*** (cons new-node nodes**))
		(edges*** (cons then-edge (cons else-edge edges**))))
	   (list new-node option.none nodes*** edges*** assertions**)))

	((equal (car stm) 'while-statement)
	 (let* ((test-exp (expression-to-acl2-expression* (l.find stm ':test)))
		(test-vars (expression-free-vars test-exp))
		(n-name (node-name))
		(live-vars (list-union (node.live-vars to-go-to)
				       (list-union test-vars (statement-free-vars (l.find stm ':body)))))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))

		(res-body (statement-to-acl2-statement* (l.find stm ':body) new-node (option.some t) nodes edges assertions end-node))
		(body-node (car res-body))
		(enter-loop-head* (cadr res-body))
		(nodes* (caddr res-body))
		(edges* (cadddr res-body))
		(assertions* (cadddr (cdr res-body)))

		(body-stm (make-trivial-stm live-vars defdata (node.live-vars body-node) (node.defdata body-node)))
		(other-stm (make-trivial-stm live-vars defdata (node.live-vars to-go-to) (node.defdata to-go-to)))

		(test-func
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    ,(use-fill-in-context 'input (list.map #'car live-vars) test-exp)))
		(neg-test-func
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    ,(use-fill-in-context 'input (list.map #'car live-vars) (list 'not test-exp))))

		(body-edge (edge.make (edge-name) new-node body-node test-func body-stm (l.find (l.find stm ':test) ':loc)
				      :enter-loop-head enter-loop-head*
				      :control (option.some 'condition-true) 
				      :source-code (option.some (expression-to-source-code (l.find stm ':test)))))
		(fall-through-edge (edge.make (edge-name) new-node to-go-to neg-test-func other-stm (l.find (l.find stm ':test) ':loc)
					      :enter-loop-head enter-loop-head 
					      :control (option.some 'condition-false) 
					      :source-code (option.some (string-append "!" (expression-to-source-code (l.find stm ':test))))))
							 
		(nodes** (cons new-node nodes*))
		(edges** (cons body-edge (cons fall-through-edge edges*))))
	   (list new-node (option.some t) nodes** edges** assertions*)))
	 
	((equal (car stm) 'do-statement)
	 (error 'unimplemented))
	((equal (car stm) 'for-statement)
	 (error 'unimplemented))
	((equal (car stm) 'declaration-statement)
	 (let* ((init-exp (expression-to-acl2-expression* (l.find stm ':initializer)))
		(init-vars (expression-free-vars (l.find stm ':initializer)))

		(type (type-to-acl2-type (l.find (l.find stm ':declaring) ':type)))
		(var (var.acl2-var (l.find (l.find stm ':declaring) ':var)))
		
		(n-name (node-name))
		(live-vars (list.remove (list var type)
					(list-union (node.live-vars to-go-to) init-vars)))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))

		(declare-stm
		 `(definec ,(statement-name) (input ,(name-to-type (defdata.name defdata)))
		    ,(name-to-type (defdata.name (node.defdata to-go-to)))
		    ,(use-fill-in-context 'input (list.map #'car live-vars)
					  `(let ((,var ,init-exp))
					     ,(cons 'list (list.map #'(lambda (var)
									(if (equal var 'counter)
									    '(+ counter 1)
									  var))
								    (list.map #'car (node.live-vars to-go-to))))))))

		(condition
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    t))

		(declare-edge (edge.make (edge-name) new-node to-go-to condition declare-stm (l.find stm ':loc)
					 :enter-loop-head enter-loop-head :source-code (option.some (statement-to-source-code stm))))
		(nodes* (cons new-node nodes))
		(edges* (cons declare-edge edges)))
	   (list new-node option.none nodes* edges* assertions)))

	((equal (car stm) 'assignment-statement)
	 (let* ((rhs-exp (expression-to-acl2-expression* (l.find stm ':expression)))
		(rhs-vars (expression-free-vars (l.find stm ':expression)))

		(lvalue (l.find stm ':lvalue))
		(var (var.acl2-var (l.find lvalue ':var)))
		(type (type-to-acl2-type (l.find (l.find stm ':expression) ':type)))
		
		(n-name (node-name))
		(live-vars (list-union (list.remove (list var type) (node.live-vars to-go-to))
				       rhs-vars))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))

		(assign-stm
		 `(definec ,(statement-name) (input ,(name-to-type (defdata.name defdata)))
		    ,(name-to-type (defdata.name (node.defdata to-go-to)))
		    ,(use-fill-in-context 'input (list.map #'car live-vars)
					  `(let ((,var ,rhs-exp))
					     ,(cons 'list (list.map #'(lambda (var)
									(if (equal var 'counter)
									    '(+ counter 1)
									  var))
								    (list.map #'car (node.live-vars to-go-to))))))))

		(condition
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    t))		

		(assign-edge (edge.make (edge-name) new-node to-go-to condition assign-stm (l.find stm ':loc)
					:enter-loop-head enter-loop-head :source-code (option.some (statement-to-source-code stm))))
		(nodes* (cons new-node nodes))
		(edges* (cons assign-edge edges)))
	   (list new-node option.none nodes* edges* assertions)))
	((equal (car stm) 'assert-statement)
	 (let* ((assert-exp (expression-to-acl2-expression* (l.find stm ':expression)))
		(assert-vars (expression-free-vars (l.find stm ':expression)))

		(n-name (assert-node-name))
		(live-vars (list-union (node.live-vars to-go-to)
				       assert-vars))
		(defdata (create-defdata live-vars))
		(new-node (node.make n-name live-vars defdata))

		(assert-stm (make-trivial-stm live-vars defdata (node.live-vars to-go-to) (node.defdata to-go-to)))

		(condition
		 `(definec ,(condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
		    t))		

		(assert-edge (edge.make (edge-name) new-node to-go-to condition assert-stm (l.find stm ':loc)
					:enter-loop-head enter-loop-head :source-code (option.some (statement-to-source-code stm))))
		(nodes* (cons new-node nodes))
		(edges* (cons assert-edge edges))
		(assertions* (map.add-exn assertions n-name assert-exp)))
	   (list new-node option.none nodes* edges* assertions*)))
	))

(defun c-defs (architecture)
  `((defconst *c-unsigned-int-size* (expt 2 ,architecture))
    (defdata c-unsigned-int (range integer (0 <= _ < *c-unsigned-int-size*)))
    (defdata c-int integer)
    (defdata c-bool boolean)
    (defdata c-char character)
    (definec c-unsigned-int-+ (x :c-unsigned-int y :c-unsigned-int) :c-unsigned-int
      (mod (+ x y) *c-unsigned-int-size*))
    (definec c-unsigned-int-- (x :c-unsigned-int y :c-unsigned-int) :c-unsigned-int
      (mod (- x y) *c-unsigned-int-size*))
    (definec c-unsigned-int-* (x :c-unsigned-int y :c-unsigned-int) :c-unsigned-int
      (mod (* x y) *c-unsigned-int-size*))
    (definec c-unsigned-int-/ (x :c-unsigned-int y :c-unsigned-int) :c-unsigned-int
      (if (equal y 0)
	  0
	(mod (truncate x y) *c-unsigned-int-size*)))
    (definec c-int-+ (x :c-int y :c-int) :c-int
      (+ x y))
    (definec c-int-- (x :c-int y :c-int) :c-int
      (- x y))
    (definec c-int-* (x :c-int y :c-int) :c-int
      (* x y))
    (definec c-int-/ (x :c-int y :c-int) :c-int
      (if (equal y 0)
	  0
	(truncate x y)))))

(defparameter *c-nondet-defs*
  '((defdata listof-c-unsigned-int (listof c-unsigned-int))
    (defdata listof-c-int (listof c-int))
    (defdata listof-c-bool (listof c-bool))
    (defdata listof-c-char (listof c-char))
    (defdata nondet (list listof-c-unsigned-int
			  listof-c-int
			  listof-c-bool
			  listof-c-char))
    (definec nondet-c-unsigned-int (nondet :nondet counter :nat) :c-unsigned-int
      (if (>= counter (length (car nondet)))
	  (mod (* counter 47) 107)
	(nth counter (car nondet))))
    (definec nondet-c-int (nondet :nondet counter :nat) :c-int
      (if (>= counter (length (cadr nondet)))
	  (- (mod (* counter 91) 103) -60)
	(nth counter (cadr nondet))))
    (definec nondet-c-bool (nondet :nondet counter :nat) :c-bool
      (if (>= counter (length (caddr nondet)))
	  (equal (mod counter 2) 0)
	(nth counter (caddr nondet))))
    (definec nondet-c-char (nondet :nondet counter :nat) :c-char
      (if (>= counter (length (cadddr nondet)))
	  #\R
	(nth counter (cadddr nondet))))))

; [32 63 U] -> '_
(defun acl2s-admit-c-defs (architecture verbose)
  (declaim (sb-ext:muffle-conditions cl:warning))
  (acl2s-event '(set-ignore-ok t))
  (list.iter #'(lambda (event)
		 (if (not (car (acl2s-event event)))
		     (if verbose (print (list 'admitted event)) '_)
		   (if verbose (print (list 'failed event)) '_)))
	     (c-defs architecture))
  (list.iter #'(lambda (event)
		 (if (not (car (acl2s-event event)))
		     (if verbose (print (list 'admitted event)) '_)
		   (if verbose (print (list 'failed event)) '_)))
	     *c-nondet-defs*))

; expression -> ACl2-expression
(defun expression-to-ACL2-expression* (exp)
  (cond ((equal (car exp) 'literal-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(value (l.find exp ':value))
		(body value))
	   body))

	((equal (car exp) 'id-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(var (var.ACL2-var (l.find exp ':var)))
		(body var))
	   body))
	
	((equal (car exp) 'unary-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(op (unary-operator-to-acl2-unary-operator (l.find exp ':operator)))
		(body1 (expression-to-acl2-expression* (l.find exp ':operand)))
		(body `(,op ,body1)))
	   body))

	((equal (car exp) 'binary-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type)))
		(op (binary-operator-to-acl2-binary-operator (l.find exp ':operator)))
		(body1 (expression-to-acl2-expression* (l.find exp ':operand1)))
		(body2 (expression-to-acl2-expression* (l.find exp ':operand2)))
		(body `(,op ,body1 ,body2)))
	   body))

	((equal (car exp) 'nondeterministic-expression)
	 (let* ((type (type-to-acl2-type (l.find exp ':type))))
	   (cond ((equal type 'c-bool)
		  '(nondet-c-bool nondet counter))
		 ((equal type 'c-char)
		  '(nondet-c-char nondet counter))
		 ((equal type 'c-unsigned-int)
		  '(nondet-c-unsigned-int nondet counter))
		 ((equal type 'c-int)
		  '(nondet-c-int nondet counter)))))

	((equal (car exp) 'conditional-expression)
	 (error 'unimplemented))
	))

#|
node = (list node-name [(list ACL2-var ACL2-type) list] [(list ACL2-var ACL2-type) list] ACL2-defdata)
edge = (list edge-name node node ACL2-definec ACL2-definec loc
             [bool option] [['condition-true 'condition-false U] option] [source-code option]
|#

(defun node.make (name live-vars defdata)
  (list name live-vars defdata))
(defun node.name (node)
  (car node))
(defun node.live-vars (node)
  (cadr node))
(defun node.defdata (node)
  (caddr node))

(defun edge.make (name node-from node-to condition stm loc &key (enter-loop-head option.none) (control option.none) (source-code option))
  (list name node-from node-to condition stm loc enter-loop-head control source-code))
(defun edge.name (edge)
  (car edge))
(defun edge.from (edge)
  (cadr edge))
(defun edge.to (edge)
  (caddr edge))
(defun edge.condition (edge)
  (cadddr edge))
(defun edge.statement (edge)
  (cadddr (cdr edge)))
(defun edge.loc (edge)
  (cadddr (cddr edge)))
(defun edge.enter-loop-head (edge)
  (cadddr (cdddr edge)))
(defun edge.control (edge)
  (cadddr (cdddr (cdr edge))))
(defun edge.source-code (edge)
  (cadddr (cdddr (cddr edge))))

; [(list ACL2-var ACL2-type) list] -> ACL2-defdata
(defun create-defdata (live-vars)
  `(defdata ,(defdata-name) ,(cons 'list (list.map #'cadr live-vars))))

; ACL2-expression [ACL2-var list] ACL2-expression -> ACL2-expression
(defun use-fill-in-context (exp args-to-pull-out hole)
  (let* ((cdr-list (cdr (list.map #'(lambda (_) 'cdr) args-to-pull-out)))
	 (pull-outs (pair.first (list.foldr
				 #'(lambda (var acc)
				     (let* ((acc-list (pair.first acc))
					    (cdr-list (pair.second acc))
					    (cdr-d-result (list.foldr
							   #'(lambda (x acc) (cons x (list acc)))
							   exp cdr-list)))
				       (pair.make (cons `(,var (car ,cdr-d-result))
							acc-list)
						  (cdr cdr-list))))
				 (pair.make () cdr-list) args-to-pull-out))))
    `(let* ,pull-outs
       ,hole)))

; make the datadefs line up, essentially a projection
(defun make-trivial-stm (live-vars1 defdata1 live-vars2 defdata2)
  ; require subset
  ;(assert (list.forall #'(lambda (e2) (list.in e live-vars1)) live-vars2))
  `(definec ,(statement-name) (input ,(name-to-type (defdata.name defdata1))) ,(name-to-type (defdata.name defdata2))
     ,(use-fill-in-context 'input
			   (list.map #'car live-vars1)
			   (cons 'list (list.map #'(lambda (var) (if (equal var 'counter)
								     '(+ counter 1)
								   var))
						 (list.map #'car live-vars2))))))

; expression [ACL2-var var map] -> [ACL2-var var map]
(defun expression-name-map (exp map)
  (cond ((equal (car exp) 'literal-expression)
	 map)
	((equal (car exp) 'id-expression)
	 (map.add-overwrite map
			    (var.ACL2-var (l.find exp ':var))
			    (var.program-var (l.find exp ':var))))
	((equal (car exp) 'unary-expression)
	 (expression-name-map (l.find exp ':operand) map))
	((equal (car exp) 'binary-expression)
	 (expression-name-map (l.find exp ':operand1)
			      (expression-name-map (l.find exp ':operand2) map)))
	((equal (car exp) 'nondeterministic-expression)
	 map) ;(list (list 'nondet 'nondet) (list 'counter 'nat)))
	((equal (car exp) 'conditional-expression)
	 (expression-name-map (l.find exp ':test)
			      (expression-name-map (l.find exp ':then)
						    (expression-name-map (l.find exp ':else) map))))
	))

; statement [ACL2-var var map] -> [ACL2-var var map]
(defun statement-name-map (stm map)
  (cond ((equal (car stm) 'compound-statement)
	 (list.foldr (lambda (stm acc)
		       (statement-name-map stm acc))
		     map (l.find stm ':statements)))
	((equal (car stm) 'if-then-statement)
	 (expression-name-map (l.find stm ':test)
			      (statement-name-map (l.find stm ':then) map)))
	((equal (car stm) 'if-then-else-statement)
	 (expression-name-map (l.find stm ':test)
		     (statement-name-map (l.find stm ':then)
				 (statement-name-map (l.find stm ':else) map))))
	((equal (car stm) 'while-statement)
	 (expression-name-map (l.find stm ':test)
			      (statement-name-map (l.find stm ':body) map)))
	((equal (car stm) 'do-statement)
	 (error 'unimplemented))
	((equal (car stm) 'for-statement)
	 (error 'unimplemented))
	((equal (car stm) 'break-statement)
	 map)
	((equal (car stm) 'continue-statement)
	 map)
	((equal (car stm) 'declaration-statement)
	 (map.add-overwrite (expression-name-map (l.find stm ':initializer) map)
			    (var.ACL2-var (l.find (l.find stm ':declaring) ':var))
			    (var.program-var (l.find (l.find stm ':declaring) ':var))))
			    
	((equal (car stm) 'assignment-statement)
	 (map.add-overwrite (expression-name-map (l.find stm ':expression) map)
			    (var.acl2-var (l.find (l.find stm ':lvalue) ':var))
			    (var.program-var (l.find (l.find stm ':lvalue) ':var))))
	((equal (car stm) 'expression-statement)
	 (error 'unimplemented))
	))



