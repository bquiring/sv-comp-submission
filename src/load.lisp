(load "src/acl2s-interaction.lisp")
(quiet-mode-on)

(acl2s-ld "src/itest-cgen.lisp")
(acl2s-ld "src/itest-ithm.lisp")

(load "src/base.lisp")
(load "src/conjecture-generation-v2.lisp")

(setf *print-case* :downcase)

(load "src/c-reasoner-utils.lisp")
(load "src/c-parser.lisp")
(load "src/intermediate-language.lisp")
(load "src/c-source.lisp")
(load "src/generate-witness.lisp")
(load "src/generate-and-prove-invariants.lisp")

;(declaim (sb-ext:muffle-conditions cl:warning))
;(declaim (sb-ext:muffle-conditions cl:style-warning))
