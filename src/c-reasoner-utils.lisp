
; [string list] -> string
(defun string-append-all (list-of-strings)
  (format nil "~{~A~}" list-of-strings))


(defun node-name ()
  (gentemp "node-"))
(defun assert-node-name ()
  (gentemp "assert-node-"))
(defun edge-name ()
  (gentemp "edge-"))
(defun condition-name ()
  (gentemp "cond-"))
(defun statement-name ()
  (gentemp "stm"))
(defun defdata-name ()
  (gentemp "data"))
(defun var-name ()
  (gentemp "var"))


(define-condition find-not-found (error)
  ((text :initarg :text :reader text)))

(define-condition find-length-error (error)
  ((text :initarg :text :reader text)))

(define-condition find-not-found-local (error)
  ((text :initarg :text :reader text)))

(define-condition find-length-error-local (error)
  ((text :initarg :text :reader text)))

(defun l.find-acc (sexp* token*)
  (if (endp sexp*)
      (error 'find-not-found-local)
    (if (equal (car sexp*) token*)
	(if (endp (cdr sexp*))
	    (error 'find-length-error-local)
	  (cadr sexp*))
      (l.find-acc (cdr sexp*) token*))))

; 'a ['a list] -> 'a
(defun l.find (sexp token)
  (handler-case (l.find-acc sexp token)
    (find-not-found-local (arg) (progn (print (list 'looking 'for token 'in sexp))
				       (error 'find-not-found)))
    (find-length-error-local (arg) (progn (print (list 'length 'error 'for token 'in sexp))
					  (error 'find-length-error)))))

