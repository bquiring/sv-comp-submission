
Instructions for SV-COMP 2020
=============================

0. Install the dependencies:

   # apt-get install cmake git make python sbcl ant 

1. Obtain and install ACL2 and ACL2s. This can be done via

   $ ./scripts/install.sh

2. Use benchexec tool to run the tests or use `run-gacal.py`
   The `--witness` option must be included.

   You can use the `run-gacal.py` script to verify each single test-case.

   $ run-gacal.py --witness WITNESS TASK

   WITNESS:      Path to a file, where the witness trace in XML will be written
   TASK:         Path to a C program to be verified
    
   For example,

   $ python run-gacal.py "test/test-program-1.i" --witness "test/test-program-1.graphml"

   $ python run-gacal.py "test/test-program-2.i" --witness "test/test-program-2.graphml"
