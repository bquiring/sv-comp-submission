#!/usr/bin/python


#print(os.path.dirname(os.path.realpath(__file__)))

import sys
import os
import subprocess
import argparse
import hashlib

#if sys.version_info[0] < 3:
#    raise Exception("Must be using Python 3")


parser = argparse.ArgumentParser()
parser.add_argument("-v","--version", action='version', version='1.0')
parser.add_argument("--witness", dest="witness")
parser.add_argument("--architecture", dest="architecture")
parser.add_argument("testcase")
args = parser.parse_args()

architecture = 32 # default
input_file = args.testcase
output_file = args.witness

# hash the input
hash_value = ""
with open(input_file,"rb") as f:
    bytes = f.read() # read entire file as bytes
    hash_value = hashlib.sha256(bytes).hexdigest();
    



# call the java sexp parser

path = os.path.dirname(os.path.realpath(__file__))

sexp_output_file = path + "/parser/" + os.path.basename(input_file) + ".sexp"

sexp_parser_call = [path + "/parser/scripts/sexp.sh", "-source", input_file, "-output", sexp_output_file]
if (sys.version_info[0] < 3) or (sys.version_info[0] == 3 and sys.version_info[1] < 5):
    subprocess.call (sexp_parser_call)
else:
    subprocess.run (sexp_parser_call)

#os.system(" ".join(sexp_parser_call))



# call ACL2s to solve

acl2s_call = ["acl2s",
              "-input-file", input_file,
              "-sexp-input-file", sexp_output_file,
              "-output-file", output_file,
              "-architecture", str(architecture),
              "-input-hash", hash_value,
              "<", path + "/src/start-script.lisp"]


os.system(" ".join(acl2s_call))

#if (sys.version_info[0] < 3) or (sys.version_info[0] == 3 and sys.version_info[1] < 5):
#    subprocess.call(acl2s_call)
#else:
#   subprocess.run(acl2s_call)









